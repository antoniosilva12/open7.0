# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Via Personal (<http://viapersonal.com.br>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, osv
from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta



class faturamento_via(osv.osv):
    _name = 'res.partner'
    _inherit = 'res.partner'

    _columns = {
        'produto_fat': fields.many2one('product.product', 'Faturar produto'),
        'venc_data': fields.integer('Data de Vencimento'),
        'parc_install': fields.integer('Parcelas da instalaçao')
    }

    def criar_pedido(self, cr, uid, ids, partner, contrato, context=None):
        pedido_obj = self.pool.get('sale.order')
        dict = {'product_id': partner.produto_fat.id,
                'name': partner.produto_fat.name,
                'price_unit': partner.produto_fat.list_price}
        pedido_id = pedido_obj.create(cr, uid, {'partner_id': partner.id,
                                                'project_id': contrato.id,
                                                'order_line': [(0, 0, dict)],
                                                'pricelist_id': 1,
                                                'partner_invoice_id': partner.id,
                                                'partner_shipping_id': partner.id,
                                                'state': 'draft'
                                                })
        pedido = pedido_obj.browse(cr, uid, pedido_id, context=context)
        return pedido

    def prepare_invoice_line_prop(self, cr, uid, ids, venc, contrato, partner, pedido, context=None):

        inst = date.today()
        prop = venc - inst
        mensalidade = (pedido.order_line[0].price_unit / 12)


        dict4 = {'product_id': pedido.order_line[0].product_id.id,
                 'name': 'Dias proporcionais',
                 'account_analytic_id': contrato.id,
                 'quantity': prop.days,
                 'price_unit': mensalidade/30
                 }

        return (0,0 , dict4)


    def prepare_invoice_line_inst(self,cr, uid, ids, contrato, partner, context=None):
        product_obj = self.pool.get('product.product')
        install = product_obj.browse(cr, uid, 4, context=context)

        a = install.list_price
        b = partner.parc_install
        instalacao = a/b
        dict3 = {'product_id': install.id,
                 'name': install.name,
                 'account_analytic_id': contrato.id,
                 'price_unit': instalacao
                 }
        return (0,0 , dict3)

    def prepare_invoice_line_plano(self, cr, uid, ids, contrato, partner, pedido, context=None):

        dict2 = {'product_id': pedido.order_line[0].product_id.id,
                 'name': pedido.order_line[0].product_id.name,
                 'account_analytic_id': contrato.id,
                 'price_unit': pedido.order_line[0].price_unit / 12
                 }

        return [(0,0, dict2)]
    def get_comment(self, cr, uid, ids, i, invoice_line, context=None):
        comment = "Boleto %d/12\n" % (i+1)
        for il in invoice_line:
            if 'quantity' in il[2]:
                qtd = il[2]['quantity']
                preco = il[2]['price_unit']
                comment = comment + " %d Dias proporcionais: R$ %.2f\n" % (qtd, preco*qtd)
            else:
                nome = il[2]['name']
                preco = il[2]['price_unit']
                comment = comment + " %s: R$ %.2f\n" % (nome, preco)


        return comment

    def criar_fatura(self, cr, uid, ids, contrato,  partner, pedido, context=None):
        fatura_id = []
        fatura_obj = self.pool.get('account.invoice')

        inst = date.today()

        venc = inst + relativedelta(days=partner.venc_data)
        if partner.venc_data < inst.day:
            venc = inst + relativedelta(day=partner.venc_data, months=1)

        for i in range(0, 12):

            invoice_line = self.prepare_invoice_line_plano(cr, uid, ids, contrato, partner, pedido, context=context)
            instal = date.today()
            if instal.day != partner.venc_data and i == 0:
                invoice_line.append(self.prepare_invoice_line_prop(cr, uid, ids, venc, contrato, partner, pedido, context=context))
            if i < partner.parc_install:
                invoice_line.append(self.prepare_invoice_line_inst(cr, uid, ids, contrato, partner, context=context))
            dados_adic = self.get_comment(cr, uid, ids, i, invoice_line, context=context)
            fatura_id.append(fatura_obj.create(cr, uid, {'partner_id': partner.id,
                                                'partner_shipping_id': partner.id,
                                                'state': 'draft',
                                                'comment': dados_adic,
                                                'account_id': 1,
                                                'date_invoice': venc,
                                                'invoice_line': invoice_line}))
            venc = venc + relativedelta(months=1)
        return fatura_id

    def faturar_produto(self, cr, uid, ids, context=None):
        partner = self.browse(cr, uid, ids, context=context)[0]
        project_obj = self.pool.get('project.project')
        project_id = project_obj.create(cr, uid, {'name': partner.name,
                                                  'partner_id': partner.id})
        projeto = project_obj.browse(cr, uid, project_id, context=context)
        contrato = projeto.analytic_account_id
        pedido = self.criar_pedido(cr, uid, ids, partner, contrato, context=context)
        fatura_id = self.criar_fatura(cr, uid, ids, contrato, partner, pedido, context=context)

        res = {}

        return res


faturamento_via()



