# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Via Personal (<http://viapersonal.com.br>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import fields, osv




class cliente_via(osv.osv):
    _name = 'cliente.via'
    _rec_name = 'Nome'

    def _get_endereco(self, cr, uid, ids, name, arg, context=None):
        res = {}
        for reg in self.browse(cr, uid, ids, context=context):
            res[reg.id] = reg.endereco + '\n' + reg.rua + '\n' + reg.numero + '\n' + reg.estado + '\n' + reg.cidade + '\n' + reg.pais

        return res

    _columns = {
        'Nome': fields.char('Nome', size=128, required=True, select=True), 'empresa_id': fields.char('Empresa', size=128),
        'empresa_cat': fields.char('Categoria da empresa', size=128, selectable=True, required=False),
        'uma_empresa': fields.boolean('uma empresa', selectable=True), 'image': fields.binary('image'),
        'endereco': fields.char('Bairro', size=128), 'rua': fields.char('Rua', size=128, selectable=True),
        'numero': fields.char('numero', size=24), 'cidade': fields.char('Cidade', size=128, selectable=True),
        'estado': fields.char('Estado', size=128, selectable=True), 'pais': fields.char('Pais', size=128),
        'cep': fields.char('CEP', size=24), 'website': fields.char('Website', size=64),
        'cargo': fields.char('Cargo', size=128), 'fone': fields.char('Telefone', size=64),
        'celular': fields.char('Celular', size=64), 'fax': fields.char('Fax', size=64),
        'email': fields.char('email', size=240, selectable=True), 'titulo': fields.char('Titulo', size=64),
        'comentario': fields.text('comentarios'),
        'end_list': fields.one2many('cliente.endereco', 'cliente_id', 'Lista de endereço'),
        'active_end': fields.many2one('cliente.endereco', 'Bairro'),
        # 'end_text': fields.function('_get_endereco', type='float', method=True, string='Endereco2')
        'resume_end': fields.function(_get_endereco, type='text', method=True, string='Endereco resumido')
    }

    def onchange_cliente_end(self, cr, uid, ids, active_end, context=None):
        res = {'value': {}}
        end_obj = self.pool.get("cliente.endereco")
        end_id = end_obj.browse(cr, uid, active_end, context=context)
        res['value'].update({'endereco': end_id.endereco, 'rua': end_id.rua,
                             'numero': end_id.numero,'cidade': end_id.cidade,'estado': end_id.estado, 'cep': end_id.cep})

        return res


cliente_via()


class cliente_endereco(osv.osv):

    _name = 'cliente.endereco'
    _rec_name = 'endereco'
    _columns = {
        'endereco': fields.char('Bairro', size=128),'rua': fields.char('Rua', size=128, selectable=True),
        'numero': fields.char('numero', size=24), 'cidade': fields.char('Cidade', size=128, selectable=True),
        'estado': fields.char('Estado', size=128, selectable=True), 'pais': fields.char('Pais', size=128),
        'cep': fields.char('CEP', size=24),
        'cliente_id': fields.many2one('cliente.via', 'Bairro', size=128, selectable=True)
    }


cliente_endereco()


